ScriptName ImmersiveTomes extends ReferenceAlias

Event OnInit()
	Debug.Notification("Immersive Spell Tomes Initialized!")
  Debug.Trace("Immersive Spell Tomes Initialized!")
EndEvent

Event OnItemRemoved(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akDestContainer)
  Book itemBook = akBaseItem as Book
  if itemBook != None && itemBook.getSpell() != None && akItemReference == None && akDestContainer == None
      Game.getPlayer().AddItem(akBaseItem, 1, true)
  endIf
endEvent
