IMMERSIVE SPELL TOMES
Never eat your tomes again! Just a simple mod to add immersion to spell tomes by preventing them from disappearing upon being used. (They're simply added back to the inventory)

:: REQUIREMENTS::
* SKSE

:: AUTHOR ::
* Aristine_ a.k.a Ari
